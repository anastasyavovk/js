		
		var number = document.querySelectorAll('.number');
		var inp = document.querySelector('.calculator__inp');
		var sign  = document.querySelectorAll('.sign');
		var result = document.querySelector('.result');
		var percent = document.querySelector('.percent');
		var clear = document.querySelector('.clear');
		var toggleSign = document.querySelector('.toggleSign');

        // var out = document.querySelector('.screen p');

		clear.addEventListener('click', function (){
			inp.value = "";
		});

		percent.addEventListener("click", function (){
			inp.value = inp.value / 100;
		});

		toggleSign.addEventListener("click", function (){
			inp.value = inp.value * -1;
		})

		for(i=0;i<number.length;i++){
			number[i].addEventListener('click', insertValue);
		}

		for(i=0;i<sign.length;i++){
			sign[i].addEventListener('click', insertValue);
		}

		function insertValue(){
			var val = String(inp.value);
			if (isNaN(this.innerText)==false){
				inp.value += this.innerText;
				return;
			}

			var symbols = "-/+*.";
			if (val.length>0) {
				var lastChar = val[val.length-1];
				if(symbols.includes(lastChar)){
					return;
				}
			}

			if(this.innerText == "."){
				var res = val.split(/[\+\-\/\*]/g);
				var lastOperand=val;

				if(res.length>1){
					lastOperand=res[res.length-1];
				}

				if(lastOperand.includes(".")){
					return;
				}
			}

			inp.value += this.innerText;
		}

		result.addEventListener('click',getResult);

		function getResult(){
			inp.value = eval(inp.value)
		}

		document.body.addEventListener('keydown',function(e){
			if (e.keyCode == 13){
				getResult();
			}
		})

		function clearAll(){
			inp.value = "";
		}

		document.body.addEventListener('keydown', (event) => {
			if (event.keyCode === 32 || event.keyCode === 8) {
				clearAll();
			}
		  });
		