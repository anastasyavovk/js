//constructor

let selectedPrice = 0;
let ingerdientsPrice = 0;

let itemSrc = null;
function drag(event){
  // itemSrc = event.target.src;
  itemSrc = event.target;
}

function drop(event){
  var img = document.createElement('img');
  const priceIng = itemSrc.parentNode.childNodes[5].innerHTML;
  img.src = itemSrc.src;
  var price = parseInt(priceIng);
  ingerdientsPrice = ingerdientsPrice+price
  
  document.querySelector('.circle').appendChild(img);
  var totalText = document.getElementById('total');
  //console.dir(totalText)
  //var curValue = parseInt(totalText.innerHTML);
  //curValue = curValue + price;
  totalText.innerHTML = selectedPrice+ingerdientsPrice + " грн";
}

function allowDrop(event){
  event.preventDefault();
}

function sizeChecked(clicked_id) {
  var allBtn = document.querySelectorAll('.custom-radio-btn');
  allBtn.forEach(el => {
    el.classList.remove("checked");
  })
  var radioBtn = document.getElementById(clicked_id);
  var price = parseInt(radioBtn.innerText.split("\n")[1])
  selectedPrice = price;
  radioBtn.classList.add("checked");
  var totalText = document.getElementById('total');
  totalText.innerHTML = selectedPrice+ingerdientsPrice + " грн";
}

//dark mode
var checkbox = document.getElementById("darkmode-toggle");
if (sessionStorage.getItem("mode") == "dark") {
  darkmode(); 
} else {
  nodark();
}

checkbox.addEventListener("change", function() {
  if (checkbox.checked) {
    darkmode();
  } else {
    nodark(); 
  }
});

function darkmode() {
  document.body.classList.add("dark-mode");
  checkbox.checked = true; 
  sessionStorage.setItem("mode", "dark");
}

function nodark() {
  document.body.classList.remove("dark-mode"); 
  checkbox.checked = false;
  sessionStorage.setItem("mode", "light");
}

// add item to the order (in cart.html)

const cards = document.querySelectorAll('.type');
const cartSide = document.querySelector('.shop-cart');
const storage = JSON.parse(localStorage.getItem('cart') || '[]');
const drink = document.querySelectorAll('.drinks');
const desert = document.querySelectorAll('.deserts');

if (storage.length){
  storage.forEach(el => {
    const {id, img, title, price} = el
    const newCard = document.createElement('div')
    newCard.innerHTML = `<li class="type">${img}<h2>${title}</h2><p id="cart-price">${price}</p><button id=${id} class="btn-del" type="button">Видалити</button>`
    // newCard.innerHTML = `<table>
    // <tr>
    //   <th><li class="type">${img}</th>
    //   <th><h2>${title}</h2></th>
    //   <th><p id="cart-price">${price}</p></th>
    //   <th><button id=${id} class="btn-del" type="button">Видалити</button></th>
    // </tr>`
    if(cartSide !== null)
    cartSide.appendChild(newCard);
  })
}

//new pizza constructor
const pizzaNew = document.querySelectorAll('.constructor-total');
console.dir(pizzaNew)

pizzaNew.forEach(el => {
  const addNewPizza = el.childNodes[3];

  addNewPizza.addEventListener('click', () => {
    const pizzaNew = document.querySelector('.constructor-left-bg');
    var id = parseInt(localStorage.getItem('id')) || 0;
    const cartStorage = localStorage.getItem('cart') || '[]'
    const cart = JSON.parse(cartStorage)
    const img = pizzaNew.outerHTML;
    const title = "Піца за Вашим рецептом";
    const price = selectedPrice + ingerdientsPrice + " грн";

    const card = {id, img, title, price}
    id = id+1;
    localStorage.setItem('id', id);
    localStorage.setItem('cart', JSON.stringify([...cart , card]))
    updateCartCount();
    showModal()
  })
})

// deserts
desert.forEach(el => {
  console.dir(el)
  const img = el.childNodes[1].outerHTML;
  const title = el.childNodes[3].innerText;
  const price = el.childNodes[5].innerText;
  const btn = el.childNodes[7];

  btn.addEventListener('click', () => {
    var id = parseInt(localStorage.getItem('id')) || 0;
    const cartStorage = localStorage.getItem('cart') || '[]'
    const cart = JSON.parse(cartStorage)
    const card = {id, img, title, price }
    id = id+1;
    localStorage.setItem('id', id);
    localStorage.setItem('cart', JSON.stringify([...cart , card]))
    updateCartCount();
    showModal()
  })
})

//drinks
drink.forEach(el => {
  console.dir(el)
  const img = el.childNodes[1].outerHTML;
  const title = el.childNodes[3].innerText;
  const price = el.childNodes[5].innerText;
  const btn = el.childNodes[7];

  btn.addEventListener('click', () => {
    var id = parseInt(localStorage.getItem('id')) || 0;
    const cartStorage = localStorage.getItem('cart') || '[]'
    const cart = JSON.parse(cartStorage)
    const card = {id, img, title, price }
    id = id+1;
    localStorage.setItem('id', id);
    localStorage.setItem('cart', JSON.stringify([...cart , card]))
    updateCartCount();
    showModal()
  })
})

//pizza
cards.forEach((el, idx) => {
  console.dir(el)
  const img = el.childNodes[1].outerHTML;
  const title = el.childNodes[3].innerText;
  const price = el.childNodes[5].innerText;
  const btn = el.childNodes[9];

  btn.addEventListener('click', () => {
    var id = parseInt(localStorage.getItem('id')) || 0;
    const cartStorage = localStorage.getItem('cart') || '[]'
    const cart = JSON.parse(cartStorage)
    const card = {id, img, title, price }
    id = id+1;
    localStorage.setItem('id', id);
    localStorage.setItem('cart', JSON.stringify([...cart , card]))
    updateCartCount();
    showModal()
  })
})

// delete item from order (from cart.html)
const btnAll = document.querySelectorAll('.btn-del');
btnAll.forEach(el => {
  el.addEventListener('click', () => {
      const cartStorage = localStorage.getItem('cart') || '[]'
      const cart = JSON.parse(cartStorage)
      for(var i = 0; i<cart.length; i++){
        if(cart[i]['id'] == el['id']){
          cart.splice(i, 1)
          break
        } 
      }
      localStorage.setItem('cart', JSON.stringify([...cart ]))
      location.reload()
      updateCartCount();
     
  })
})

// btn size pizza event
var btnM = document.getElementById('btn-size2');
if(btnM){
  btnM.click();
}

//sum price
var sumPrice = 0;
var pricesInCart = document.querySelectorAll('#cart-price');
pricesInCart.forEach(el => {
  sumPrice = sumPrice + parseInt(el.innerHTML);
})
var totalPr = document.getElementById('sum-price');
if(totalPr){
  totalPr.innerHTML = sumPrice + " грн";
}

//counter items
function updateCartCount(){
  const cartStorage = localStorage.getItem('cart') || '[]'
  const cart = JSON.parse(cartStorage)
  var c = document.querySelector('.counter');
  c.innerHTML = cart.length;
}
updateCartCount();

//modal add to cart
function showModal(){
  var modal = document.getElementById('modal');
  var span = document.getElementsByClassName("close")[0];
  modal.style.display = "block";
  span.onclick = function() {
    modal.style.display = "none";
  }
}
  
//form order
function showForm(){
  var forma = document.querySelector('.forma');
  var span = document.getElementsByClassName("close")[0];

  const cartStorage = localStorage.getItem('cart') || '[]'
  const cart = JSON.parse(cartStorage);
  if(cart.length ==0){
    sorry.style.display = "block"; //sorry modal
    return
  }
  forma.style.display = "block";
  document.getElementById("order").style.filter = "blur(10px)";
  span.onclick = function() {
    forma.style.display = "none";
    document.getElementById("order").style.filter = "none";
  }
}

var btnForm = document.getElementById('to-form');
if(btnForm){
btnForm.addEventListener('click', showForm);
}

//modal thanks
function showModalThanks(){

var r1 = checkRequired([username, email]);
var r2 = checkLength(username,3,15);
var r3 = checkEmail(email);

if(r1 && r2 && r3)
{
  var thanks = document.getElementById('thanks');
  var span = document.getElementsByClassName("close")[0];
  thanks.style.display = "block";
  span.onclick = function() {
    thanks.style.display = "none";
  }
}
}

var orderBtn = document.getElementById('check-btn');
if(orderBtn){
orderBtn.addEventListener('click', showModalThanks)
}
if(orderBtn){
orderBtn.addEventListener('click', function(e){
  localStorage.removeItem('cart');
  var cartCont = document.querySelector('.shop-cart');
  cartCont.innerHTML = "Корзина пуста";
})
}


//validation form check order
const form = document.getElementById('form');
const username = document.querySelector('.username');
const email = document.getElementById('email');

function showError(input, message) {
    const formControl = input.parentElement;
    formControl.className = 'form-control error';
    const small = formControl.querySelector('small');
    small.innerText = message;
}

function showSucces(input) {
    const formControl = input.parentElement;
    formControl.className = 'form-control success';
}

function checkEmail(input) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(re.test(input.value.trim())) {
        showSucces(input)
    }else {
        showError(input,'Email невірний');
        return false
    }
    return true
}

//перевірка на обовязкові поля
function checkRequired(inputArr) {
    inputArr.forEach(function(input){
        if(input.value.trim() === ''){
            showError(input,`${getFieldName(input)} is required`)
            return false
        }else {
            showSucces(input);
        }

    });
    return true
}

//check input Length
function checkLength(input, min ,max) {
    if(input.value.length < min) {
        showError(input, `${getFieldName(input)} має бути не менше ${min}`);
        return false
    }else if(input.value.length > max) {
        showError(input, `${getFieldName(input)} має бути не більше ${max}`);
        return false
    }else {
        showSucces(input);
    }
    return true
}

//get FieldName
function getFieldName(input) {
    return input.id.charAt(0).toUpperCase() + input.id.slice(1);
}
