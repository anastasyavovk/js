const url = `https://api.nasa.gov/planetary/apod?api_key=FpdmDYgcPYRb8wtCJxpMBtwTZumyXMHW74t1tQUR`;

const image = document.querySelector('.pictureofday');
const caption = document.querySelector('.picture');

fetch(url)
  .then(response => {
  return response.json();
})
  .then(data => {
  console.log(data);
  image.src = data.url;
  caption.innerHTML = data.explanation;
});
